package com.userapi.dto;

import java.util.Objects;

public class UserDTO {
    private int id;
    private String fullname;
    private String phone;
    private String email;
    private Integer quantity_order;

    public Integer getQuantity_order() {
        return quantity_order;
    }

    public void setCount(Integer count) {
        this.quantity_order = quantity_order;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserDTO)) return false;
        UserDTO userDTO = (UserDTO) o;
        return getId() == userDTO.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", fullname='" + fullname + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public UserDTO(int id, String fullname, String phone, String email, Integer quantity_order) {
        this.id = id;
        this.fullname = fullname;
        this.phone = phone;
        this.email = email;
        this.quantity_order = quantity_order;
    }
}
