package com.userapi.model;


import javax.persistence.*;
import java.io.Serializable;

import com.userapi.dto.UserDTO;
import org.hibernate.annotations.NamedNativeQuery;
@Entity
@Table(name = "users")
@NamedNativeQuery( name = "find_most_user", query = "select users.*, count(orders.user_id) count from orders \n" +
        "\tjoin users on orders.user_id = users.id \n" +
        "            group by orders.user_id\n" +
        "            order by  count(orders.user_id) desc limit 1", resultSetMapping = "mapUserDTO")

@SqlResultSetMapping(
        name= "mapUserDTO",
        classes =  @ConstructorResult(
                targetClass = UserDTO.class,
                columns = {
                        @ColumnResult(name="id", type=Integer.class),
                        @ColumnResult(name="fullname", type=String.class),
                        @ColumnResult(name="phone", type=String.class),
                        @ColumnResult(name="email", type=String.class),
                        @ColumnResult(name="count", type = Integer.class)
                }
        )
)

public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "fullname")
    private String fullname;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name = "role_id")
    private int role_id;

    public User(int id, String fullname, String phone, String email) {
        this.id = id;
        this.fullname = fullname;
        this.phone = phone;
        this.email = email;
    }

    public User(int id, String fullname, String username, String password, String phone, String email, int role_id) {
        this.id = id;
        this.fullname = fullname;
        this.username = username;
        this.password = password;
        this.phone = phone;
        this.email = email;
        this.role_id = role_id;
    }

    public User() {

    }

    public int  getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }
}
