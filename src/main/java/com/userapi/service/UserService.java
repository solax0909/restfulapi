package com.userapi.service;

import com.userapi.dto.UserDTO;
import com.userapi.model.User;

import java.util.List;


public interface UserService {
    List<User> fillAllUser();

    void addUser(User user);
    void deleteUser(Integer id);
    void updateUser(String str, Integer id);
    List<UserDTO> mostOrder();
}
